package com.example.practika;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public com.example.practika.MainActivity context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
    }
    public void onMyButtonClick(View view) {
        int dl = 0;
        EditText myEditText = findViewById(R.id.editText);
        String vvodLogin = myEditText.getText().toString();
        EditText myEditText2 = findViewById(R.id.editText2);
        String vvodParol = myEditText2.getText().toString();
        String [] h1 = getResources().getStringArray ( R.array.logins);
        String [] h2 = getResources().getStringArray ( R.array.parols );
        dl = h1.length;
        if ((vvodParol.trim().length()!=0)&&(vvodLogin.trim().length()!=0)) {
            for (int i = 0; i < dl; i++) {
                if ((h1[i].equals(vvodLogin)) && (h2[i].equals(vvodParol))) {
                    Intent myIntent = new Intent(context, Main2Activity.class);
                    myIntent.putExtra("myLogin", vvodLogin);
                    startActivity(myIntent);
                    break;
                } else
                {
                    if ((h1[i].equals(vvodLogin)))
                    {
                    Toast toast = Toast.makeText(this, "Пароль введен неверно!", Toast.LENGTH_LONG);
                    toast.show();
                } else if (i == dl-1){
                        Toast toast = Toast.makeText(this, "Логин и пароль введены неверно!", Toast.LENGTH_LONG);
                        toast.show();
                    }
                }

            }
        }
        else {
            Toast toast = Toast.makeText(this, "Пожалуйста, заполните все поля!!", Toast.LENGTH_LONG);
            toast.show();
        }

        }
        }


